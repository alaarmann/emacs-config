(require 'package)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-backends
   (quote
    (company-tern company-bbdb company-eclim company-semantic company-clang company-xcode company-cmake company-capf company-files
		  (company-dabbrev-code company-gtags company-etags company-keywords)
		  company-oddmuse company-dabbrev)))
 '(company-idle-delay 0)
 '(company-minimum-prefix-length 1)
 '(company-selection-wrap-around t)
 '(delete-selection-mode t)
 '(desktop-restore-eager 5)
 '(desktop-save-mode t)
 '(eshell-visual-commands (quote ("vi" "screen" "top" "less" "more" "lynx" "ncftp")))
 '(exec-path-from-shell-check-startup-files nil)
 '(js2-mode-show-parse-errors nil)
 '(js2-mode-show-strict-warnings nil)
 '(package-archive-priorities (quote (("melpa-stable" . 10) ("gnu" . 5) ("melpa" . 0))))
 '(package-archives
   (quote
    (("gnu" . "https://elpa.gnu.org/packages/")
     ("melpa-stable" . "https://stable.melpa.org/packages/")
     ("melpa" . "https://melpa.org/packages/"))))
 '(package-pinned-packages
   (quote
    ((helm-lsp . "melpa")
     (lsp-ui . "melpa")
     (company-lsp . "melpa")
     (lsp-treemacs . "melpa")
     (lsp-mode . "melpa")
     (elm-mode . "melpa")
     (scss-mode . "melpa")
     (use-package . "melpa")
     (helm-company . "melpa")
     (yasnippet . "melpa"))))
 '(package-selected-packages
   (quote
    (ox-gfm evil flycheck-pest pest-mode ob-rust rust-mode lsp-haskell ox-reveal orgit graphviz-dot-mode typescript-mode pipenv rjsx-mode js2-mode leuven-theme pandoc-mode scss-mode use-package helm-lsp lsp-ui company-lsp lsp-treemacs lsp-mode elm-mode helm-company yasnippet web-mode helm-projectile helm easy-kill markdown-mode flycheck projectile company-tern company json-mode yaml-mode exec-path-from-shell prettier-js magit haskell-mode)))
 '(safe-local-variable-values (quote ((setq projectile-test-cmd . "yarn test"))))
 '(tool-bar-mode nil))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(load-theme 'leuven t)

(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(setq
   backup-by-copying t
   backup-directory-alist
    '(("." . "~/.emacs.d/backup"))
   delete-old-versions t
   kept-new-versions 6
   kept-old-versions 2
   version-control t)

(use-package projectile
  :ensure t
  :pin melpa-stable
  :defer t
  :config
  (define-key projectile-mode-map (kbd "C-s-p") 'projectile-command-map)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (projectile-mode +1))

(add-to-list 'default-frame-alist '(fullscreen . maximized))
(global-set-key (kbd "<C-prior>") 'previous-buffer)
(global-set-key (kbd "<C-next>") 'next-buffer)
(global-set-key (kbd "<C-return>") (lambda ()
                   (interactive)
                   (end-of-line)
                   (newline)))
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
;; (add-hook 'js2-mode-hook #'js2-imenu-extras-mode)
(add-hook 'js2-mode-hook 'prettier-js-mode)
(add-hook 'after-init-hook #'global-flycheck-mode)
(add-hook 'js2-mode-hook (lambda ()
;;                            (tern-mode)
                           (company-mode)))
;; (setq tern-command '("tern" "--no-port-file"))
(show-paren-mode 1)
(global-set-key [remap kill-ring-save] #'easy-kill)
(global-set-key [remap mark-sexp] #'easy-mark)

(use-package helm
  :defer t
  :config
  (helm-mode 1)
  (define-key global-map [remap find-file] 'helm-find-files)
  (define-key global-map [remap occur] 'helm-occur)
  (define-key global-map [remap list-buffers] 'helm-buffers-list)
  (define-key global-map [remap dabbrev-expand] 'helm-dabbrev)
  (define-key global-map [remap execute-extended-command] 'helm-M-x)
  (define-key global-map [remap apropos-command] 'helm-apropos)
  (unless (boundp 'completion-in-region-function)
    (define-key lisp-interaction-mode-map [remap completion-at-point] 'helm-lisp-completion-at-point)
    (define-key emacs-lisp-mode-map       [remap completion-at-point] 'helm-lisp-completion-at-point)))

(use-package helm-projectile
  :defer t
  :init
  (helm-projectile-on))

(eval-after-load 'company
  '(progn
     (define-key company-mode-map (kbd "C-:") 'helm-company)
     (define-key company-active-map (kbd "C-:") 'helm-company)))

(use-package lsp-mode
  :hook
  ((js2-mode . lsp-deferred)
  (web-mode . lsp-deferred)
  (css-mode . lsp-deferred)
  (scss-mode . lsp-deferred)
  (elm-mode . lsp-deferred)
  (python-mode . lsp-deferred)
  (rust-mode . lsp-deferred)
  (haskell-mode . lsp)
  (typescript-mode . lsp-deferred))
  :commands (lsp lsp-deferred)
  :bind ("M-#" . lsp-rename)
  :config
  ;; (setq lsp-auto-configure nil)
  (setq lsp-log-io nil)
  (setq lsp-prefer-flymake nil))

(use-package lsp-ui
  :commands lsp-ui-mode
  :bind (
	 ("M-?" . lsp-ui-peek-find-references)
	 ("M-*" . lsp-ui-doc-glance))
  :config
  (setq lsp-ui-sideline-enable nil)
  (setq lsp-ui-doc-enable nil)
  (setq lsp-ui-peek-fontify (quote never)))

(use-package company-lsp
  :commands company-lsp
  :config
  (push 'company-lsp company-backends))

(use-package helm-lsp :commands helm-lsp-workspace-symbol)
(use-package lsp-treemacs :commands lsp-treemacs-errors-list)

(use-package lsp-haskell
  :ensure t
  :pin melpa
  :config
  (setq lsp-haskell-process-path-hie "stack")
  (setq lsp-haskell-process-args-hie '("exec" "ghcide" "--" "--lsp"))
  (setq lsp-haskell-process-wrapper-function (lambda (argv) (cons (car argv) (cddr argv)))))


(use-package elm-mode
  :ensure t
  :bind (:map elm-mode-map
         ("M-." . lsp-ui-peek-find-definitions))
  :hook ((elm-mode . company-mode)
	 (elm-mode . elm-format-on-save-mode)))

(defun enable-minor-mode (my-pair)
  "Enable minor mode if filename match the regexp.  MY-PAIR is a cons cell (regexp . minor-mode)."
  (if (buffer-file-name)
      (if (string-match (car my-pair) buffer-file-name)
	  (funcall (cdr my-pair)))))

(use-package web-mode
  :ensure t
  :defer t
  :mode (("\\.html?\\'" . web-mode)
         ("\\.tsx\\'" . web-mode))
  :config
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-enable-auto-closing t)
  (add-hook 'web-mode-hook #'(lambda ()
                            (enable-minor-mode
                             '("\\.tsx\\'" . prettier-js-mode)))))

(use-package scss-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.scss\\'" . scss-mode)))

(use-package magit
  :ensure t
  :defer t)
(global-set-key (kbd "C-c g") 'magit-file-popup)
(global-set-key (kbd "C-s-g") 'magit-file-popup)
(global-set-key (kbd "C-x g") 'magit-status)

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "pandoc")
  :hook (markdown-mode . pandoc-mode))

(use-package pandoc-mode
  :ensure t
  :hook (pandoc-mode . pandoc-load-default-settings))

(use-package pipenv
  :ensure t
  :hook (python-mode . pipenv-mode))

(use-package typescript-mode
  :ensure t
  :pin melpa
  :defer t
  :hook (typescript-mode . prettier-js-mode))

(use-package ox-latex)
(use-package ox-beamer)
(use-package ox-reveal
  :ensure t)
(use-package ox-gfm
  :ensure t)

(use-package ob-rust
  :ensure t)

(use-package org
  :requires (ox-latex ox-beamer)
  :config
  (org-babel-do-load-languages 'org-babel-load-languages
                           '((emacs-lisp . t)
                             (dot . t)
                             (python . t)
                             (rust . t)
                             (shell . t)))
  (setq org-export-allow-bind-keywords t)
  (setq org-latex-listings 'minted)
  (add-to-list 'org-latex-packages-alist '("" "minted"))
  (setq org-latex-pdf-process
      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f")))

(use-package graphviz-dot-mode
  :ensure t
  :config
  (setq graphviz-dot-indent-width 4))

(use-package company-graphviz-dot)

(use-package org-present
  :bind (("C-c o p" . org-present))
  :load-path "~/.emacs.d/elisp/org-present"
  :hook ((org-present-mode . (lambda ()
                 (org-display-inline-images)
                 (org-present-hide-cursor)
		 (org-present-read-only)))
         (org-present-mode-quit . (lambda ()
                 (org-remove-inline-images)
                 (org-present-show-cursor)
		 (org-present-read-write)))))

(use-package rust-mode
  :ensure t
  :pin melpa
  :defer t
  :config
  (setq indent-tabs-mode nil)
  (setq rust-format-on-save t))

(use-package flycheck-pest
  :ensure t
  :pin melpa
  :defer t
  :init
  (flycheck-pest-setup))

(use-package pest-mode
  :ensure t
  :pin melpa
  :defer t
  :mode "\\.pest\\'")

(use-package evil
  :ensure t
  :pin melpa
  :config
  (evil-mode 1))

;;
;;
